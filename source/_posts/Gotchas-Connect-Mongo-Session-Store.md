title: 'Gotchas, Connect-Mongo Session Store'
author: Phaninder Pasupula
date: 2019-01-13 23:12:37
tags:
---

[Connect-Mongo](https://github.com/kcbanner/connect-mongo) module which is commonly used for session store for a MongoDb backend. This post is an extension for my previous post [MongoDB/Mongoose connect best practices](/mongodb-error-trying-to-open-unclosed-connection). In this post, I would like to elaborate on good practices in using session store.

**Initialization**

When using mongoose with connect-mongo you need to initialize the session store after mongoose connects.

{% gist 9463004 mongostore_conn.js %}
<br />

If not this might lead to the below errors.

> Error:Trying to open unclosed connection.
> 
> Error: Error setting TTL index on collection : sessions

**Enable auto reconnect**

It also takes the param `auto_reconnect: true`. This will be passed as constructor to MongoDb server as the auto\_reconnect option. By setting this you can avoid the below error

> Error: no open connections

Sample set-up can be found [here](https://gist.github.com/pasupulaphani/9463004#file-connect-mongo_session_store-js).

{% gist 9463004 connect-mongo_session_store.js %}


**Use existing Mongoose or Mongo native connection**

Yes, instead of creating a new Db object, you can just reuse the **DB object** or **a connection** already instantiated by mongoose!
{% gist 9463004 connect-mongo_use_mongoose_conn1.js %}
{% gist 9463004 connect-mongo_use_mongoose_conn2.js %}

Also make sure that `mongoose.connection.db` is fully initialized before used for session store otherwise it fails with TTL for ‘sessions’ error.

**Using connect events**

It is common practice to start app initialization with connection open event. If you use ‘open’ or ‘connecting’ events, there is a good possibility that the connection might not be fully initialized by the time you use it for session store. Make sure that you use appropriate events like ‘connected’ before you use existing mongoose connection. If not you might encounter above errors. Below is from a sample from my [other post](/mongodb-error-trying-to-open-unclosed-connection)

{% gist 9463004 mongoose_connet.js %}
