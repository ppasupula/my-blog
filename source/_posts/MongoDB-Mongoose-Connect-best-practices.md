title: MongoDB/Mongoose Connect best practices
author: Phaninder Pasupula
date: 2019-01-13 23:24:33
tags:
---
It is important how to handle your MongoDb connections in your app. Basically when you fail to close an connection and try to open it instantaneously, you probably encounter this error. It can be while retrieving data, running tests, creating sessions.

> Error:Trying to open unclosed connection.

As the error says you are trying to open an unclosed connection with MongoDb. Few things to know before you start with Mongoose to avoid these type of errors:

*   Do not open a connection for each web request.
*   Open it and leave it open until you close/shutdown your app.
*   Use createConnection instead of mongoose.connect if needed to connect to several different databases

#### [](#Mongoose-connect-sample "Mongoose connect sample")Mongoose connect sample

{% gist 9463004 mongoose_connet.js %}


*   Use [mongoose.createConnection](http://mongoosejs.com/docs/api.html#index_Mongoose-createConnection) to create a new connection instance if needed.
*   For long running applications if you see `"connection closed"` errors try enable `keepAlive`

`mongoose.connect` opens a pool of connections that concurrent requests can share. You shouldn’t be connecting and disconnecting on each request. Instead, connect during your application start up and disconnect during shutdown.

#### [](#Mongoose-response-hooks "Mongoose response hooks")Mongoose response hooks

Sometimes when you are streaming huge data you may need to close connections after every request. For this you can use after response events/hooks.

{% gist 9463004 after_res_hooks.js %}


#### [](#Connect-mongo-session-store-tips "Connect-mongo session store tips")Connect-mongo session store tips

[connect-mongo](https://github.com/kcbanner/connect-mongo) module which is commonly used for session store takes the param `auto_reconnect: true`. This will be passed as constructor to MongoDb server as the auto\_reconnect option.

_When using mongoose with connect-mongo you need to initialize the session store after mongoose connects._

Sample set-up can be found [here](https://gist.github.com/pasupulaphani/9463004#file-connect-mongo_session_store-js).

**Use existing mongoose connection**

Yes, instead of creating a new Db object, you can just reuse the db object already instantiated by mongoose!

{% gist 9463004 connect-mongo_use_mongoose_conn1.js %}
{% gist 9463004 connect-mongo_use_mongoose_conn2.js %}


#### [](#Mocha-Tests "Mocha Tests")Mocha Tests

You don’t have to open a new connection for each test, connect once at the top level of your test and re-use the connection

{% gist 9463004 mocha_tests.js %}

